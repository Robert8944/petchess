import java.awt.*;

import javax.swing.*;
public class Gameboard extends JFrame{
	private static final long serialVersionUID = 1L;
	//private member variables
	Square[][] squarray;
	PawnAsker askie = new PawnAsker(this);
	Square first;
	String nps= "Queen";
	public static boolean turn=true;
	Image i = Toolkit.getDefaultToolkit().getImage("whitecursor.gif");
	Cursor w =Toolkit.getDefaultToolkit().createCustomCursor(i, new Point(0,0), "name");
	Image j = Toolkit.getDefaultToolkit().getImage("blackcursor.gif");
	Cursor b =Toolkit.getDefaultToolkit().createCustomCursor(j, new Point(0,0), "name"); 
	
	//constructor
	public Gameboard(){
		super("CHESS!!!!!!!!!!!!");
		//birth the array
		squarray  = new Square[8][8];
		this.setLayout(new GridLayout(8,8));
		//birth EACH square
		for(int i=0; i<squarray.length; i++){//row
			for(int j=0; j<squarray[i].length; j++){//col
				squarray[i][j] = new Square(i,j,this);
				if ((squarray[i][j].getrow()+squarray[i][j].getcol())%2==1){
					squarray[i][j].setBackground(Color.BLACK);
				}
				else{
					squarray[i][j].setBackground(Color.WHITE);
				}
				this.add(squarray[i][j]);


			}
		}
		
		
		
		this.setCursor(w);
		
		
		
		//white
		//white pawns
		squarray[1][0].setPiece(new Pawn(squarray[1][0],"whitepawn.gif",ChessPiece.WHITE));
		squarray[1][1].setPiece(new Pawn(squarray[1][1],"whitepawn.gif",ChessPiece.WHITE));
		squarray[1][2].setPiece(new Pawn(squarray[1][2],"whitepawn.gif",ChessPiece.WHITE));
		squarray[1][3].setPiece(new Pawn(squarray[1][3],"whitepawn.gif",ChessPiece.WHITE));
		squarray[1][4].setPiece(new Pawn(squarray[1][4],"whitepawn.gif",ChessPiece.WHITE));
		squarray[1][5].setPiece(new Pawn(squarray[1][5],"whitepawn.gif",ChessPiece.WHITE));
		squarray[1][6].setPiece(new Pawn(squarray[1][6],"whitepawn.gif",ChessPiece.WHITE));
		squarray[1][7].setPiece(new Pawn(squarray[1][7],"whitepawn.gif",ChessPiece.WHITE));
		
		//white rooks
		squarray[0][0].setPiece(new Rook(squarray[0][0],"whiterook.gif",ChessPiece.WHITE));
		squarray[0][7].setPiece(new Rook(squarray[0][7],"whiterook.gif",ChessPiece.WHITE));
		
		//white knights
		squarray[0][1].setPiece(new Knight(squarray[0][1],"whiteknight.gif",ChessPiece.WHITE));
		squarray[0][6].setPiece(new Knight(squarray[0][6],"whiteknight.gif",ChessPiece.WHITE));
		
		//white bishops
		squarray[0][2].setPiece(new Bishop(squarray[0][2],"whitebishop.gif",ChessPiece.WHITE));
		squarray[0][5].setPiece(new Bishop(squarray[0][5],"whitebishop.gif",ChessPiece.WHITE));
		
		//white king
		squarray[0][3].setPiece(new King(squarray[0][3],"whiteking.gif",ChessPiece.WHITE));
		
		//white queen
		squarray[0][4].setPiece(new Queen(squarray[0][4],"whitequeen.gif",ChessPiece.WHITE));
		
		//black
		//black pawns
		squarray[6][0].setPiece(new Pawn(squarray[6][0],"blackpawn.gif",ChessPiece.BLACK));
		squarray[6][1].setPiece(new Pawn(squarray[6][1],"blackpawn.gif",ChessPiece.BLACK));
		squarray[6][2].setPiece(new Pawn(squarray[6][2],"blackpawn.gif",ChessPiece.BLACK));
		squarray[6][3].setPiece(new Pawn(squarray[6][3],"blackpawn.gif",ChessPiece.BLACK));
		squarray[6][4].setPiece(new Pawn(squarray[6][4],"blackpawn.gif",ChessPiece.BLACK));
		squarray[6][5].setPiece(new Pawn(squarray[6][5],"blackpawn.gif",ChessPiece.BLACK));
		squarray[6][6].setPiece(new Pawn(squarray[6][6],"blackpawn.gif",ChessPiece.BLACK));
		squarray[6][7].setPiece(new Pawn(squarray[6][7],"blackpawn.gif",ChessPiece.BLACK));
		
		//black rooks
		squarray[7][7].setPiece(new Rook(squarray[7][7],"blackrook.gif",ChessPiece.BLACK));
		squarray[7][0].setPiece(new Rook(squarray[7][0],"blackrook.gif",ChessPiece.BLACK));
		
		//black knights
		squarray[7][1].setPiece(new Knight(squarray[7][1],"blackknight.gif",ChessPiece.BLACK));
		squarray[7][6].setPiece(new Knight(squarray[7][6],"blackknight.gif",ChessPiece.BLACK));
		
		//black bishops
		squarray[7][2].setPiece(new Bishop(squarray[7][2],"blackbishop.gif",ChessPiece.BLACK));
		squarray[7][5].setPiece(new Bishop(squarray[7][5],"blackbishop.gif",ChessPiece.BLACK));
		
		//black king
		squarray[7][3].setPiece(new King(squarray[7][3],"blackking.gif",ChessPiece.BLACK));
		
		//black queen
		squarray[7][4].setPiece(new Queen(squarray[7][4],"blackqueen.gif",ChessPiece.BLACK));
		
		//finish up
		this.setVisible(true);
		this.setSize(500,500);
		
		askie.setVisible(false);
	}

	public void clicked(Square s){
		
		if((first==null && s.getPiece()!=null && s.getPiece().team==Gameboard.turn)||(first!=null)){
		if(first != null && s.getPiece()!=null && first.getPiece().team == s.getPiece().team){//finds if the second one that you clicked is your own teammate
			
			if ((first.getrow()+first.getcol())%2==1){//sets your color back to normal
				first.setBackground(Color.BLACK);
			}
			else{
				first.setBackground(Color.WHITE);
			}
		
			first=s;
			first.setBackground(Color.GRAY);
			return;
		}
		//phase 1
		//JOptionPane.showMessageDialog(this, "you clicked ("+s.getrow()+","+s.getcol()+")");
		//if (first == null){
		//	first=s;
		//}
		//else{
		//	JOptionPane.showMessageDialog(this, "you moved from ("+first.getrow()+","+first.getcol()+") to ("+s.getrow()+","+s.getcol()+")");
		//
		//}
		
		
		
		//if (turn)
		//	turn=false;
		//else if(!turn)
		//	turn=true;
		//if (first==null){
		//	if(turn  && !s.getPiece().team){
		//		JOptionPane.showMessageDialog(this, "It's not your turn!");
		//		return;
		//	}
		//	if(!turn  && s.getPiece().team){
		//		JOptionPane.showMessageDialog(this, "It's not your turn!");
		//		return;
		//  }
		//}
		
		
		if (s.getPiece()!= null && first==null){
				first = s;
				first.setBackground(Color.GRAY);
		}
		
			else if(s.getPiece()!= null && first!=null){
				first.getPiece().move(s);
				if(!this.hasKings() ){
					@SuppressWarnings("unused")
					VicWin Winner= new VicWin();
					this.dispose();
				}
				if ((first.getrow()+first.getcol())%2==1){
					first.setBackground(Color.BLACK);
				}
				else{
					first.setBackground(Color.WHITE);
				}
			
				first=null;
			
			
			}
			else if(s.getPiece()==null && first!= null){
				first.getPiece().move(s);
					if ((first.getrow()+first.getcol())%2==1){
						first.setBackground(Color.BLACK);
					}
					else{
						first.setBackground(Color.WHITE);
					}
				
					first=null;
				
				
			}
		}
		else{
			if(first!=null)
			
				
			if ((first.getrow()+first.getcol())%2==1){
				first.setBackground(Color.BLACK);
			}
			else{
				first.setBackground(Color.WHITE);
			}
			if (s.getPiece()!=null){
				JOptionPane.showMessageDialog(this, "Its's not your turn!");
			}
			
			first=null;
		}
	
	
		
	}
	public boolean isUnblocked(Square start, Square end){
	
		int row=start.getrow();
		int col=start.getcol();
		int Dr= end.getrow()-start.getrow();
		int Dc= end.getcol()-start.getcol();
		if(Math.abs(Dc)!=0){
			Dc=Dc/Math.abs(Dc);
		}
		else{//makes sure it wont be zero
			Dc=0;
		}
		if(Math.abs(Dr)!=0){
			Dr=Dr/Math.abs(Dr);
		}
		else{
			Dr=0;
		}
		
		row = row+Dr;
		col = col+Dc;
		while(row!=end.getrow()||col!=end.getcol()){//while your not at the end
			if(squarray[row][col].getPiece()==null){
					col+=Dc;
				
					row+=Dr;
			}
			else{
				
				return false;
			}
			
			
		}
		
		return true;
		
		
		
	}
	public void promote(ChessPiece Cp){
		//the name of the new piece as a string
		ChessPiece np=null;//the new piece
		boolean t = true;//the new pieces team
		if(Cp.team==true){//sets your team
			t=true;
		}
		if(Cp.team==false){
			t=false;
		}
		askie.ask();//asks him
		
		if (nps.equals("Queen")){
			if(t==true)
				np= new Queen (Cp.home,"whitequeen",true);
			if(t==false)
				np= new Queen (Cp.home,"blackqueen",false);
		}
		if (nps.equals("Bishop")){
			if(t==true)
				np= new Bishop (Cp.home,"whiteBishop",true);
			if(t==false)
				np= new Bishop (Cp.home,"blackbishop",false);
			
		}	
		if (nps.equals("Knight")){
			if(t==true)
				np= new Knight (Cp.home,"whiteknight",true);
			if(t==false)
				np= new Knight (Cp.home,"blackknight",false);
			
		}
		if (nps.equals("Rook")){
			if(t==true)
				np= new Rook (Cp.home,"whiterook",true);
			if(t==false)
				np= new Rook (Cp.home,"blackrook",false);
			
		}
		
		
		
		Cp=np;
		
		
	}
	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, "Maximize the window and remember that white(dogs) always goes first!");
		new Gameboard();
		
		
	}
	public boolean hasKings(){
		int nK=0;
		for(int i = 0; i<8; i++){
			for(int j = 0; j<8; j++){
				if (squarray[i][j].getPiece()!=null && squarray[i][j].getPiece().equals("King")){
					nK++;
				}
			}
		}
		if (nK!=2){
			return false;
		}
		else
			return true;
	}

	public void blahblah(String text) {
		nps=text;
		
	}

}
