import javax.swing.JOptionPane;


public class Queen extends ChessPiece{
	public Queen(Square h, String filename, boolean t) {
		super(h, filename, t);
		
	}
	
	public boolean isMoveLegal(Square dest) {
		if(home.getcol()==dest.getcol() || home.getrow()==dest.getrow() || Math.abs(Math.abs(dest.getcol()-home.getcol())-Math.abs(dest.getrow()-home.getrow()))==0){
			if(dest==home){
				JOptionPane.showMessageDialog(Square.gb , "You can't kill yourself! Because this is... ");
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return false;
		}
		
	}

	
	public String pieceName() {
		
		return "Queen";
	}
}
