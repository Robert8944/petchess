import javax.swing.JOptionPane;


public class Rook extends ChessPiece{
	public Rook(Square h, String filename, boolean t) {
		super(h, filename, t);
		
	}
	
	public boolean isMoveLegal(Square dest) {
		if (home.getcol()==dest.getcol() || home.getrow()==dest.getrow()){
			if(dest==home){
				JOptionPane.showMessageDialog(Square.gb , "You can't kill yourself! Because this is... ");
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return false;
		}
	}

	
	public String pieceName() {
		
		return "Rook";
	}
}
