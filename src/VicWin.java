import java.awt.*;

import javax.swing.*;

public class VicWin extends JFrame{
	private static final long serialVersionUID = 1L;
	protected Image img;
	
	
	
	public VicWin(){
		super("Winner!");
		img = Toolkit.getDefaultToolkit().getImage("VicPic.gif");
		MediaTracker mt = new MediaTracker(new Component(){
			private static final long serialVersionUID = 1L;});
		mt.addImage(img,0);
		this.setVisible(true);
		this.setSize(400,75);
	}
	public void paintComponents(Graphics g){
		g.drawImage(img,0,0,this.getWidth(),this.getHeight(),null,null);
	}
}
