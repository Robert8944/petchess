import javax.swing.JOptionPane;


public class Bishop extends ChessPiece{

	public Bishop(Square h, String filename, boolean t) {
		super(h, filename, t);
		
	}
	
	public boolean isMoveLegal(Square dest) {
		if (Math.abs(Math.abs(dest.getcol()-home.getcol())-Math.abs(dest.getrow()-home.getrow()))==0){
			if(dest==home){
				JOptionPane.showMessageDialog(Square.gb , "You can't kill yourself! Because this is... ");
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return false;
		}
		
	}

	
	public String pieceName() {
		
		return "Bishop";
	}
}
