import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;

import javax.swing.JOptionPane;


public abstract class ChessPiece {
	//private member variables
	protected Square home;
	private Image img;
	protected boolean team;
	
	//i don't want to forget this
	public static final boolean BLACK = false;
	public static final boolean WHITE = true;
	
	//constructor
	public ChessPiece(Square h, String filename, boolean t){
		home = h;
		img = Toolkit.getDefaultToolkit().getImage(filename);
		MediaTracker mt = new MediaTracker(new Component(){
			private static final long serialVersionUID = 1L;});
		mt.addImage(img,0);
		try{mt.waitForAll();}catch(Exception ex){ex.printStackTrace();}
		team = t;
	}
	
	//abstract functions
	public abstract boolean isMoveLegal(Square dest);
	public abstract String pieceName();
	
	//how to draw myself
	public void draw(Graphics g){
		g.drawImage(img, 0, 0, 90, 90, null, null);
	}
	public void move(Square dest){
		
		Square old;
		if (isMoveLegal(dest)){
			
			if((this.pieceName()=="Knight")||(Square.gb.isUnblocked(this.home,dest))){
			if (Gameboard.turn==true)
				Gameboard.turn=false;
			else if (Gameboard.turn==false)
				Gameboard.turn=true;
			old=home;
			home=dest;
			old.repaint();
			home.repaint();
			dest.setPiece(this);
			old.setPiece(null);
			old.repaint();
			if (old.getPiece()!= null){
				old.setPiece(null);
			}
			if(this.pieceName()=="Pawn"&&(dest.getrow()==0||dest.getrow()==7)){
				Square.gb.promote(this);
				
			}
			if(Gameboard.turn){
				Square.gb.setCursor(Square.gb.w);
			}
			else{
				Square.gb.setCursor(Square.gb.b);
			}
			}
			else{
				JOptionPane.showMessageDialog(Square.gb,"You're blocked!!");
			}
			
		}
		else{
			JOptionPane.showMessageDialog(Square.gb,"Not a valid move!");
		}
	}
	
	
}
