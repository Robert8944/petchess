import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class PawnAsker extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	PawnAskerB B1, B2, B3, B4;
	JPanel setUp = new JPanel();
	private static Gameboard daddy;
	
	
	public PawnAsker(Gameboard d){		
		super("What do you want your pawn to become?");
		daddy = d;
		setUp.setLayout(new GridLayout(1,3));
		B1= new PawnAskerB("Queen");
		B1.addActionListener(this);
		B2= new PawnAskerB("Bishop");
		B3= new PawnAskerB("Knight");
		B4= new PawnAskerB("Rook");
		setUp.add(B1);
		setUp.add(B2);
		setUp.add(B3);
		setUp.add(B4);
		this.add(setUp);
		this.setVisible(false);
		this.setSize(400,75);
		
		
	}
	public void ask(){
		this.setVisible(true);
		
	}public void unask(){
		this.setVisible(false);
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		Object t = arg0.getSource();
		daddy.blahblah( ((PawnAskerB)t).getText() );
		
	}
	
	
		
	

}
