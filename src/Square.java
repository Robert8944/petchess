import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

public class Square extends JPanel implements MouseListener{
	private static final long serialVersionUID = 1L;
	//private Member variables
	private int row, col;
	public static Gameboard gb;
	private boolean color;
	private ChessPiece piece; //who's on me?
	
	
	
	//nickname
	private static final boolean WHITE = true;
	private static final boolean BLACK = false;
	
	//constructor!
	public Square(int r, int c, Gameboard g){
		super();
		this.addMouseListener(this);
		row = r;
		col = c;
		gb = g;
	}
	
	public int getrow(){
		return row;
	}
	public int getcol(){
		return col;
	}
	public ChessPiece getPiece(){
		return piece;
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
		
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (this.getBackground()!=Color.GRAY){
			if ((this.getrow()+this.getcol())%2==1){
				this.setBackground(new Color(200, 200, 0));
			}
			else{
				this.setBackground(Color.YELLOW);
			}
		}
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (this.getBackground()!=Color.GRAY){
			if ((this.getrow()+this.getcol())%2==1){
				this.setBackground(Color.BLACK);
			}
			else{
				this.setBackground(Color.WHITE);
			}
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		gb.clicked(this);
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		
	}
	//override paint Component!
	public void paintComponent(Graphics g){
		//draw yourself like a panel would
		super.paintComponent(g);
		//now tell my piece to draw!
		if (piece != null)//Somebody on me
			piece.draw(g);
		this.repaint();
		
	}
	public void setPiece(ChessPiece p){
		piece=p;
	}

	public void setColor(boolean color) {
		this.color = color;
	}

	public boolean getColor() {
		return color;
	}

	public static boolean isWhite() {
		return WHITE;
	}

	public static boolean isBlack() {
		return BLACK;
	}
}