
public class Pawn extends ChessPiece{
	boolean isFirstMove;
	
	public Pawn(Square h, String filename, boolean t) {
		super(h, filename, t);
		isFirstMove=true;
		
	}
	
	public boolean isMoveLegal(Square dest) {
		if (team){//if white
		if (isFirstMove){//asks if its your first move
			if (team && dest.getcol()==home.getcol() && dest.getrow()==(home.getrow()+2) && dest.getPiece()==null){//lets you move ahead two spaces on your first turn and only if no one is there
				isFirstMove=false;//makes it no longer your first move
				
				return true;//is legal
			}
		}
		if ((home.getcol()+1==dest.getcol()||home.getcol()-1==dest.getcol()) && dest.getPiece() != null && dest.getPiece().team != this.team && home.getrow()+1 == dest.getrow()){//will let you move diagonally foreward if there is a piece from the opposite team there.
			
				isFirstMove=false;//makes you first move false even if it already is
			
			return true;//is legal

		}
		
		if (home.getrow()+1==dest.getrow() && dest.getPiece()==null && dest.getcol()==home.getcol()){
			isFirstMove=false;
			return true;
		}
		else{
			
			return false;

		}
		}
		
		else{//if black
			if (isFirstMove){//asks if its your first move
				if (!team && dest.getcol()==home.getcol() && dest.getrow()==(home.getrow()-2) && dest.getPiece()==null){//lets you move ahead two spaces on your first turn and only if no one is there
					isFirstMove=false;//makes it no longer your first move
					
					return true;//is legal
				}
			}
			if ((home.getcol()+1==dest.getcol()||home.getcol()-1==dest.getcol()) && dest.getPiece() != null && dest.getPiece().team != this.team && home.getrow()-1 == dest.getrow()){//will let you move diagonally foreward if there is a piece from the opposite team there.
				
					isFirstMove=false;//makes you first move false even if it already is
				
				return true;//is legal

			}
			
			if (home.getrow()-1==dest.getrow() && dest.getPiece()==null && dest.getcol()==home.getcol()){
				isFirstMove=false;
				return true;
			}
			else{
				
				return false;

			}
			}
		
		

	}

	
	public String pieceName() {
		
		return "Pawn";
	}

}
