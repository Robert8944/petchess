import javax.swing.JOptionPane;


public class Knight extends ChessPiece{
	public Knight(Square h, String filename, boolean t) {
		super(h, filename, t);
		
	}
	
	public boolean isMoveLegal(Square dest) {
		if((Math.abs(home.getcol()-dest.getcol()) == 2 && Math.abs(home.getrow()-dest.getrow()) == 1)||(Math.abs(home.getrow()-dest.getrow()) == 2 && Math.abs(home.getcol()-dest.getcol()) == 1)){	
			return true;
		}
		else{
			if(dest==home){
				JOptionPane.showMessageDialog(Square.gb , "You can't kill yourself! Because this is...");
				return false;
			}
			else{
				return false;
			}
			
		}
	}

	
	public String pieceName() {
		
		return "Knight";
	}
}
