import javax.swing.JOptionPane;


public class King extends ChessPiece{
	public King(Square h, String filename, boolean t) {
		super(h, filename, t);
		
	}
	
	public boolean isMoveLegal(Square dest) {
		if (Math.abs(home.getcol()-dest.getcol())<=1 && Math.abs(home.getrow()-dest.getrow())<=1){
			if(dest==home){
				JOptionPane.showMessageDialog(Square.gb , "You can't kill yourself! Because this is... ");
				return false;
			}
			else{
				return true;
			}
			
		}
		else
			return false;
		
	}

	
	public String pieceName() {
		
		return "King";
	}
}
